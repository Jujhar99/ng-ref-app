import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AppComponent } from './app.component';
import { Page02Component } from './page02/page02.component';
import { Page404Component } from './page404/page404.component';

const appRoutes:Routes = [
    {path: 'hero/:id', component: Page02Component},
    {
        path: 'wassup',
        component: Page02Component
    },
    {
        path: 'heroes',
        component: Page404Component,
        data: {title: 'Heroes List'}
    },
    {
        path: '',
        redirectTo: '/wassup',
        pathMatch: 'full'
    },
    {path: '**', component: Page404Component}
];

@NgModule({
    declarations: [
        AppComponent,
        Page02Component,
        Page404Component
    ],
    imports: [
        BrowserModule,
        RouterModule.forRoot(
            appRoutes,
            {enableTracing: true} // <-- debugging purposes only
        )
    ],
    providers: [],
    bootstrap: [AppComponent]
})

export class AppModule {
}
