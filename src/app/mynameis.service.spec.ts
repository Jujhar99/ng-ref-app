import { TestBed, inject } from '@angular/core/testing';

import { MynameisService } from './mynameis.service';

describe('MynameisService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [MynameisService]
    });
  });

  it('should be created', inject([MynameisService], (service: MynameisService) => {
    expect(service).toBeTruthy();
  }));
});
