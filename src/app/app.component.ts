import { Component } from '@angular/core';
import { MynameisService } from './mynameis.service';

@Component({
    selector: 'app-root',
    templateUrl: './app.component.html',
    styleUrls: ['./app.component.css'],
    providers: [MynameisService]
})

export class AppComponent {
    title = 'flaky app';
    name = 'Bobby Bans';
    assetvalue = '1600';

    rung() {
        return "yellow";
    }

    date = '4/11/2018'

    constructor(private mynameisService:MynameisService) {}
    getBackMessage() {
        this.mynameisService.what();
    }
}


